/**
 *  Fichier permettant de définire un  suite de constantes ici  la race.
 */
export enum Enum_Race {
    elf = 'elf',
    human = 'human',
    dwarf = 'dwarf',
}