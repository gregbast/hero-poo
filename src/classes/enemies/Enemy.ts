import { Characters } from "../Characters";
import { Dragon } from "./Dragon";

export class Enemy extends Characters {
    protected fly: boolean;

    constructor(name: string, health: number, hitStrength: number, lvl: number, exp: number, fly: boolean) {
        super(name, health, hitStrength, lvl, exp)
        this.fly = fly
    }

    /*private Golem = new Enemy(this.name, 100, 30, 1, 0, false)
    private griffin = new Enemy(this.name, 100, 30, 1, 0, false)
    private Berserker = new Enemy(this.name, 100, 30, 1, 0, false)
    private Assassin = new Enemy(this.name, 100, 30, 1, 0, false)
    private Werewolf = new Enemy(this.name, 100, 30, 1, 0, false)
*/
    public getFly(): boolean {
        return this.fly;
    }

    public setFly(fly: boolean): void {
        this.fly = fly;
    }
}