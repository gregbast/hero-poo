import {Characters} from "../Characters";
import {Enum_Race} from "../enum_Race";
import {Enemy} from "../enemies/Enemy";
export class Hero extends Characters {
  protected race: Enum_Race;

  constructor(
    name: string,
    health: number,
    hitStrength: number,
    lvl: number,
    exp: number,
    race: Enum_Race,

  ) {
    super(name, health, hitStrength, lvl, exp);
    this.race = race;
  }

  public getRace(): string {
    return this.race;
  }
  public setRace(race: Enum_Race): void {
    this.race = race;
  }

  public attack( enemy: Enemy): number {
    return this.hitStrength * this.lvl;
  }

  private elf=new Hero(this.name,100,30,1,0,Enum_Race.elf);
  private human=new Hero(this.name,100,30,1,0,Enum_Race.human);
  private dwarf=new Hero(this.name,100,30,1,0,Enum_Race.dwarf);
}
  