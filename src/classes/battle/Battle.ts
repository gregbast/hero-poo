
import { Characters } from "../Characters";
import { Dragon } from "../enemies/Dragon";
import { Enemy } from "../enemies/Enemy";
import { Hero } from "../hero/Hero";
import { Enum_Race } from "../enum_Race";

export class Battle {

    fighter1: Hero;
    fighter2: Enemy;
    hitStrength: number;
    lvl: number;


    public constructor(fighter1: Hero, fighter2: Enemy) {

        this.fighter1 = fighter1;
        this.fighter2 = fighter2;

    }
    public attack(enemy: Characters): number {
        return this.hitStrength * this.lvl;

    }

    startbattle(): void {
        while (this.fighter1.getHealth() > 0 && this.fighter2.getHealth() > 0) {
            this.firstAttack()
            if (this.fighter2.getHealth() > 0 && this.fighter1.getHealth() > 0) {
                this.secondAttack()
            }

        }
    }
    //attack hero to enemy 
    firstAttack() {


        let damage: number = this.fighter1.getHitStrength()
        let damage2: number = this.fighter2.getHitStrength()
        let experience: number = this.fighter1.getExp()
        let level: number = this.fighter1.getLvl()
        let resistanceFighter2: number = this.fighter1.getHitStrength()

        //Elf : +10% de hitStrength sur les ennemis volants, -10% de hitStrength sur les ennemis au sol
        if (this.fighter1.getRace() == 'elf' && this.fighter2.getFly() == true) {
            damage = damage * 1.1
        } else if (this.fighter1.getRace() == 'elf' && this.fighter2.getFly() == false) {
            damage = (damage * 0.1) - damage
        }
        //Humans : +10% de hitStrength sur les ennemis au sol, -10% de hitStrength sur les ennemis volants
        this.fighter2.setHealth(this.fighter2.getHealth() - damage)
        if (this.fighter1.getRace() == 'human' && this.fighter2.getFly() == false) {
            damage = damage * 1.1
        } else if (this.fighter1.getRace() == 'human' && this.fighter2.getFly() == true) {
            damage = (damage * 0.1) - damage
        }
        //chaque combat gagné rapporte 2 de xp, quand l'xp atteint 10 le lvl incremente de 1
        if (this.fighter2.die()) {
            experience = experience + 2
        } else if (experience = 10) {
            level = level + 1
            experience = 0
        }



    }
    //attack enemy to hero
    secondAttack() {
        let damage: number = this.fighter1.getHitStrength()
        let damage2: number = this.fighter2.getHitStrength()
        let level: number = this.fighter1.getLvl()
        let resistanceFighter2: number = this.fighter1.getHitStrength()

        if (this.fighter2.getName() == 'Dragon') {
            damage = damage * 1.1
        }
        //Le Dragon et le Wereworf ont une resistance de 50% supplementaire
        if (this.fighter2.getName() == 'Dragon' || this.fighter2.getName() == 'werewolf') {
            resistanceFighter2 = this.fighter1.getHitStrength() * 0.5
        }
        //L'Assassin inflige 10% de degat supplementaire à chaque nouvelle attaque
        if (this.fighter2.getName() == 'Assassin') {
            damage2 = damage2 * 0.1
        }
        //Le berserker a un bonus de resistance de 30%
        if (this.fighter2.getName() == 'Berserker') {
            damage = this.fighter1.getHitStrength() * 0.3
        }
    }


}





