import { Hero } from "../Hero";
import { Enemy } from "../enemies/Enemy";
import { Golem } from "../enemies/Golem";
import { Werewolf } from "../enemies/Werewolf";
import { Griffin } from "../enemies/Griffin";
import { Assassin } from "../enemies/Assassin";
import { Battle } from "./Battle";
import { Dragon } from "../enemies/Dragon";
import { Enum_Race } from "../enum_Race";
import { Berserker } from "../enemies/Berserker";

export class BattleSimulation {

    enemyGroup: Enemy[] = [];
    hero: Hero;
    assassin: Assassin;
    berserker: Berserker;
    dragon: Dragon;
    golem: Golem;
    griffin: Griffin;
    werewolf: Werewolf;
    counter: number;
}

    
    
    