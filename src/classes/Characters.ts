/** Class representing Characters. */

export class Characters {
    /**
     *
     * @protected  represent the properties of the class Charcter
     */

    public name: string;
    public health: number;
    public hitStrength: number;
    public lvl: number;
    public exp: number;

    /**
     *
     * @param name
     * @param health
     * @param hitStrength
     * @param lvl
     * @param exp
     * Represents the parametres of the class
     */
    constructor(name: string, health: number, hitStrength: number, lvl: number, exp: number) {

        this.hitStrength = hitStrength;
        this.name = name;
        this.health = health;
        this.lvl = lvl;
        this.exp = exp;

    }

    public getName(): string {
        return this.name;
        /**
         * return a string representation of this.getName
         **/
    }

    public setName(name: string): void {
        this.name = name;
        /**
         * return a string representation of this.setName
         **/
    }

    public getHealth(): number {
        return this.health;
        /**
         * return a number representation of this.getHealth
         **/
    }

    public setHealth(health: number): void {
        this.health = health;
    }

    public getHitStrength(): number {
        return this.hitStrength;
    }

    public setHitStrength(hitStrength: number): void {
        this.hitStrength = hitStrength;
    }

    public getLvl(): number {
        return this.lvl;
    }

    public setLvl(lvl: number): void {
        this.lvl = lvl;
    }

    public getExp(): number {
        return this.exp;
    }

    public setExp(exp: number): void {
        this.exp = exp;
    }
    public attack(enemy: Characters): number {
        return this.hitStrength * this.lvl;
    }
    
    public die() {
            return this.getHealth() == 0; 
        }
    }
        
           
                
  
            
            
        

